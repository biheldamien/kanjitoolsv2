{-# LANGUAGE OverloadedStrings #-}
module KANJIDic.Parser
where

import qualified Data.Text        as T
import           KANJIDic.Types
import           Data.Attoparsec.Text
import Control.Applicative ((<|>))

parseKANJIDICLine :: Parser KANJIDICLine
parseKANJIDICLine = do
  kanji <- anyChar <?> "Kanji"
  skipWhile (==' ')
  kanjiHexCode <- parseKanjiHexCode
  skipWhile (==' ')
  infos <- parseInfoType `sepBy1'` many1(char ' ') <?> "Kanji infos"
  skipWhile (==' ' )
  onReadings <- parseKanjiOnReading  `sepBy'` many1(char ' ') <?> "Kanji on readings"
  skipWhile (==' ' )
  kunReadings <- parseKanjiKunReading  `sepBy'` many1(char ' ') <?> "Kanji kun readings"
  skipWhile (==' ' )
  skipWhile (/= '{') <?> "Skip to translations"
  translations <- parseKanjiMeaning `sepBy1'` many1(char ' ') <?> "Kanji translations"
  skipWhile (==' ' )
  return $ KANJIDICLine kanji kanjiHexCode infos onReadings kunReadings translations
  <?> "KANJIDIC Line"


parseKanjiHexCode :: Parser Integer
parseKanjiHexCode = hexadecimal <?> "Kanji hexcode"

parseInfoType :: Parser InfoType
parseInfoType = try parseBushuNumber <|> try parseHistoricalRadicalNumber <|> try parseFrequency
  <|> try parseGrade <|> try parseJLPTLevel <|> try parseHalpernIndex <|> try parseKanjiInContextIndex <|> parseUnknown <?> "Type Info"

parseUnknown :: Parser InfoType
-- TODO A REVOIR LE TAKEWHILE
parseUnknown = Unknown <$> takeWhile1 (\c-> c/='\n' && c/='\r' && c/=' ' && not(isHiragana c) && not(isKatakana c)) <?> "Unknwon field"

parseDigitAfterLetter :: Char -> Parser Int
parseDigitAfterLetter c = char c *> decimal <?> "Num afer" ++ [c]

parseBushuNumber :: Parser InfoType
parseBushuNumber = BushuNumber <$> parseDigitAfterLetter 'B'

parseHistoricalRadicalNumber :: Parser InfoType
parseHistoricalRadicalNumber = HistoricalRadicalNumber <$> parseDigitAfterLetter 'C'

parseFrequency :: Parser InfoType
parseFrequency = Frequency <$> parseDigitAfterLetter 'F'

parseGrade :: Parser InfoType
parseGrade = Grade <$> parseDigitAfterLetter 'G'

parseJLPTLevel :: Parser InfoType
parseJLPTLevel = JLPTLevel <$> parseDigitAfterLetter 'J'

parseHalpernIndex :: Parser InfoType
parseHalpernIndex = HalpernIndex <$> parseDigitAfterLetter 'H'

parseKanjiInContextIndex :: Parser InfoType
parseKanjiInContextIndex = DictionaryIndex KanjiInContext <$> (string "DJ" *> decimal)

isKatakana :: Char -> Bool
isKatakana x =  x>= 'ァ' && x<= 'ー'

isHiragana :: Char -> Bool
isHiragana x =  x>= 'ぁ' && x<= 'ゔ'

parseKanjiOnReading :: Parser T.Text
parseKanjiOnReading =  (char '-' >> takeWhile1 isKatakana ) <|> takeWhile1 isKatakana

parseKanjiKunReading :: Parser T.Text
parseKanjiKunReading = takeWhile1 (\c -> isHiragana c || c== '.' || c== '-')

parseKanjiMeaning :: Parser T.Text
parseKanjiMeaning = 
  do skip (=='{')
     res <- takeTill (=='}')
     skip (=='}')
     return res

parseFile :: Parser [KANJIDICLine]
parseFile = do
  manyTill anyChar endOfLine <?> "First line" -- Skip first line
  parseKANJIDICLine `sepBy1` (endOfInput <|> endOfLine)
