{-# LANGUAGE OverloadedStrings #-}
module KANJIDic.Anki
where

import           Data.Char      (ord)
import           Data.Maybe     (mapMaybe)
import           Data.Text      (Text)
import qualified Data.Text      as T
import           KANJIDic.Types
import           Numeric        (showHex)

kanjidicLineToAnkiLine :: KANJIDICLine -> Text
kanjidicLineToAnkiLine kl = T.intercalate ";" [
  T.pack [kanji kl],
  T.intercalate "," $ kanjiFieldMeanings kl,
  T.intercalate "," $ kanjiFieldOnReadings kl,
  T.intercalate "," $ kanjiFieldKunReadings kl,
  flip T.append ".svg\">" . T.append "<img src=\"0". kanjiToHex $ kanji kl,
  T.intercalate "," $ map (T.pack . show) $ mapMaybe getKanjiInContext  $ kanjiInfos kl]
  where
    kanjiToHex = T.pack . flip showHex "" . Data.Char.ord
    infos = kanjiInfos kl
