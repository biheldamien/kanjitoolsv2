module KANJIDic.Types
where

import           Data.Maybe (mapMaybe)
import           Data.Text  (Text)

data KANJIDICLine = KANJIDICLine {kanji::Char, hexNumber::Integer,kanjiInfos :: [InfoType],
  kanjiFieldOnReadings :: [Text],
  kanjiFieldKunReadings :: [Text],
  kanjiFieldMeanings :: [Text]} deriving (Show)

data InfoType =
   BushuNumber Int
  |HistoricalRadicalNumber Int
  |Frequency Int
  |Grade Int
  |JLPTLevel Int
  |HalpernIndex Int
  |NelsonIndex Int
  |NewNelsonIndex Int
  |DictionaryIndex DictionaryName Int
  |SKIPCode Int Int Int
  |StrokeCount Int
  |UnicodeEncoding Int
  |SpahnIndex
  |FourCornerCode {getTopLeftCode:: Int, getTopRightCode::Int,  getBottomLeftCode::Int, getBottomRightCode::Int}
  -- |?
  |HenshallIndex Int
  |GakkenIndex Int
  |HeisigIndex Int
  |NeillIndex Int
  |KoreanReading String
  |Pinyin String
  |CrossReferenceCode CrossReferenceDictionary String
  |Unknown Text 
  deriving (Show,Eq,Ord)

data DictionaryName =
   KanjiToKana
  |JapaneseForBusyPeople
  |KanjiWay
  |JapaneseKanjiFlashCards
  |Kodansha
  |AGuideToReadingAndWritingJapanese
  |KanjiInContext
  |KanjiLearnersDictionary
  |RTK
  |EssentialKanji
  |KodanshaKanjiDictionary
  |Bonjinsha
  |Sakade
  |TuttleKanjiCards
  deriving (Show, Eq, Ord)

data CrossReferenceDictionary =
   Nelson
  |JIS
  deriving (Show, Eq, Ord)

getKanjiInContext :: InfoType -> Maybe Int
getKanjiInContext (DictionaryIndex KanjiInContext a) = Just a
getKanjiInContext _                                  = Nothing
