{-#LANGUAGE OverloadedStrings #-}
module Main
where

import           Data.Char          (ord)
import Data.Maybe (mapMaybe)
import Data.List (sort,group,sortBy)
import Data.Text (Text)
import qualified Data.Text          as T
import qualified Data.Text.Lazy          as TL
import qualified Data.Text.IO       as TIO
import Data.Text.Lazy.Encoding (decodeUtf8)
import Data.Semigroup ((<>))
import           Numeric            (showHex)
import           System.FilePath
import Network.HTTP.Client
import Network.HTTP.Client.TLS
import Data.Attoparsec.Text (parseOnly)
import           KANJIDic.Anki
import           KANJIDic.Parser
import           KANJIDic.Types
import JMDict.Parser
import JMDict.Types
import Options.Applicative

filePathDic :: String
filePathDic = "./kanjidic_fr"
filePathWords :: String
filePathWords = "./JMdict.xml"
-- filePathWords = "/tmp/test.xml"

data Command = WriteAnkiLine String
              |WriteKanjisFromURL String
              |GetKanjiInContext Int Int
              |WriteWordsLines String

parseCommandLine :: Parser Command
parseCommandLine = subparser
  (command "writekanjiline" (info parseWriteAnkiOptions(progDesc "Transorm a kanji file to an importable file for Anki"))
  <> command "kanjisfromurl" (info parseWriteKanjisFromUrlOptions (progDesc "Display all kanjis from an URL"))
  <> command "getkanjiincontext" (info parserGetKanjiInContextOptions (progDesc "Display kanji in context kanjis"))
  <> command "writewordslines" (info parseWriteWordsLines (progDesc "Transform a words file to an importable file for Anki")))

parseWriteAnkiOptions :: Parser Command
parseWriteAnkiOptions =
  WriteAnkiLine <$> strArgument (metavar "FILE")
 
parseWriteKanjisFromUrlOptions :: Parser Command
parseWriteKanjisFromUrlOptions =
  WriteKanjisFromURL <$> strArgument (metavar "URL")

parserGetKanjiInContextOptions :: Parser Command
parserGetKanjiInContextOptions =
  GetKanjiInContext <$> argument auto (metavar "FROM") <*> argument auto (metavar "TO")

parseWriteWordsLines :: Parser Command
parseWriteWordsLines = 
  WriteWordsLines <$> strArgument (metavar "FILE")

filterUnnecessaryKanji  :: [String] -> [KANJIDICLine] -> [KANJIDICLine]
filterUnnecessaryKanji kanjis = filter (\x-> [kanji x]`elem`kanjis) 

writeAnkiLinesFromFile :: String -> IO ()
writeAnkiLinesFromFile sourceFilePath = do
  let outputFileName = takeDirectory sourceFilePath </> takeBaseName sourceFilePath ++ "_output" <.> takeExtension sourceFilePath
  kanjiToTransform <- fmap lines (Prelude.readFile sourceFilePath)
  getAnkiLines kanjiToTransform >>= TIO.writeFile outputFileName . T.unlines 
  mapM_ (putStrLn . \c -> showHex (Data.Char.ord (head c)) "") kanjiToTransform

getAnkiLines :: [String] -> IO [Text] 
getAnkiLines kanjis =  map kanjidicLineToAnkiLine <$> fmap (filterUnnecessaryKanji kanjis)getKanjisFromDic 

getKanjisFromDic :: IO [KANJIDICLine]
getKanjisFromDic = 
  TIO.readFile filePathDic >>= \dictionaryContent ->
    either
      error
      return
      (parseOnly KANJIDic.Parser.parseFile dictionaryContent)

-- TODO RETRAVAILLER CETTE FONCTION
isKanji :: Char -> Bool
isKanji c = c >= 'ぁ' && c <= '龯' 

writeKanjiFromURL :: String -> IO ()
writeKanjiFromURL url = do
  manager <- newManager tlsManagerSettings
  request <- parseRequest url
  response <- httpLbs request manager
  let kanjis = map ((:[]) . head) $ group $ sort $ TL.unpack $ TL.filter isKanji $ decodeUtf8 $ responseBody response
  getAnkiLines kanjis >>= mapM_ TIO.putStrLn

getKanjiInContextKanjis :: Int -> Int -> IO ()
getKanjiInContextKanjis from to =
  fmap (map kanjidicLineToAnkiLine . sortBy (\x y -> compare (mapMaybe getKanjiInContext $ kanjiInfos x) (mapMaybe getKanjiInContext $ kanjiInfos y) ) . filterKanjisInContextKanjis)  getKanjisFromDic
        >>= mapM_ TIO.putStrLn
  where
    filterKanjisInContextKanjis = filter ((\x -> not (null x) && head x>=from && head x<=to). mapMaybe getKanjiInContext . kanjiInfos)

data KanjiReading = KanjiReading String deriving (Show)

-- parseKanjiReading = do 
--   ignoreTree (/="reb")
--   tagNoAttr "reb" $ do
--     name <- content
--     return $ KanjiReading (T.unpack name)
-- 
-- parseTTT =
--   tagNoAttr "JMdict" $ do
--     ignoreTree (/="entry")
--     tagNoAttr "entry" $ do
--       ignoreTree (/="r_ele")
--       tagNoAttr "r_ele" parseKanjiReading

-- f word =  $(xq "\
--                         \ for $x in doc('/home/dbihel/Téléchargements/JMdict.xml')//entry\
--                         \ where $x/k_ele/keb = $word \
--                         \ return concat({$x/k_ele[1]/keb},':', {$x/r_ele[1]/reb}) \
--                         \ ")

main :: IO ()
main = execParser (info (helper <*> parseCommandLine)(fullDesc <> header "Tools to learn Japanese")) >>= execute
  where execute (WriteAnkiLine sourceFilePath) = writeAnkiLinesFromFile sourceFilePath
        execute (WriteKanjisFromURL url) = writeKanjiFromURL url
        execute (GetKanjiInContext from to) = getKanjiInContextKanjis from to
        -- execute (WriteWordsLines file) = do
        --   -- hiraganas <- (map XText . lines) <$> readFile file
        --   -- a <- $(xq "f($hiraganas)")
        --   -- putXSeq a
        --   jmdict <- parseJMDict "/home/dbihel/Téléchargmements/JMdict.xml"
        --   print $ filter (\x->readElement x=="ひがし") (entries jmdict)

------------Test---------------

assertEqual :: (Show a, Eq a) => a -> a -> String
assertEqual a b = show (a==b) ++ if a/=b then " Current:" ++ show a ++ " " ++ show b else []

-- lanchTest = do
--   content <- TIO.readFile filePathDic
--   let lines = 4
--   test_kanji_meaning
--   test_kanji_meanings
--   test_kanji_meanings_with_one_on
--   test_kanji_meanings_with_one_kun
--   test_kanji_meanings_with_field
--   test_kanji_meanings_with_unknown_field
--   test_kanji_meanings_with_line
--   test_kanji_meanings_with_two_lines
--   case parse parseFile "" ( T.unlines $ take lines $T.lines content) of
--     Left a-> print a
--     Right a -> putStrLn $ assertEqual (length a) lines
--
-- test_kanji_meaning =
--   case parse parseKanjiMeaning "" (T.pack "{lol} ") of
--     Left a -> print a
--     Right (KanjiFieldMeaning ( KanjiMeaning a)) -> putStrLn $ assertEqual a "lol"
--
-- test_kanji_meanings =
--   case parse (parseKanjiField `endBy` many1 space) "" (T.pack "{lol} {sdsd} {ds}          {ds}  {d}  ") of
--     Left a -> print a
--     Right a -> putStrLn $ assertEqual (fmap (fmap meaning.kanjiFieldMeaning) a) ["lol","sdsd","ds","ds","d"]
--
-- test_kanji_meanings_with_one_on =
--   case parse (parseKanjiField `endBy` many1 space) "" (T.pack "{lol} {sdsd} {ds}   かな       {ds}  {d}  ") of
--     Left a -> print a
--     Right a -> let (m,r,i) = splitKanjiFields a in do
--                 putStrLn $ assertEqual (map reading r) ["かな"]
--                 putStrLn $ assertEqual (sort $ map meaning m) (sort ["lol","sdsd","ds","ds","d"])
--
-- test_kanji_meanings_with_one_kun =
--   case parse (parseKanjiField `endBy` many1 space) "" (T.pack "{lol} {sdsd} {ds} トカ.ナ  かな       {ds}  {d}  ") of
--     Left a -> print a
--     Right a -> let (m,r,i) = splitKanjiFields a in do
--                 putStrLn $ assertEqual (sort $ map reading r) (sort ["かな","トカ.ナ"])
--                 putStrLn $ assertEqual (sort $ map meaning m) (sort ["lol","sdsd","ds","ds","d"])
--
-- test_kanji_meanings_with_field =
--   case parse (parseKanjiField `endBy` many1 space) "" (T.pack "{lol} B46     {sdsd} {ds} トカ.ナ  かな  F32     {ds}    B45  {d}  ") of
--     Left a -> print a
--     Right a -> let (m,r,i) = splitKanjiFields a in do
--                 putStrLn $ assertEqual (sort $ map reading r) (sort ["かな","トカ.ナ"])
--                 putStrLn $ assertEqual (sort $ map meaning m) (sort ["lol","sdsd","ds","ds","d"])
--                 putStrLn $ assertEqual (sort $ map info i) (sort [BushuNumber 46,BushuNumber 45, Frequency 32])
--
-- test_kanji_meanings_with_unknown_field =
--   case parse (parseKanjiField `endBy` many1 space) "" (T.pack "{lol} B46 DLFKDLFK dkfdlk D546546    {sdsd} {ds} トカ.ナ  かな  F32     {ds}    B45  {d}  ") of
--     Left a -> print a
--     Right a -> let (m,r,i) = splitKanjiFields a in do
--                 putStrLn $ assertEqual (sort $ map reading r) (sort ["かな","トカ.ナ"])
--                 putStrLn $ assertEqual (sort $ map meaning m) (sort ["lol","sdsd","ds","ds","d"])
--                 putStrLn $ assertEqual (sort $ map info i) (sort [Unknown "DLFKDLFK", Unknown "dkfdlk", Unknown "D546546", BushuNumber 46,BushuNumber 45, Frequency 32])
--
-- test_kanji_meanings_with_line =
--   case parse parseKANJIDICLine "" (T.pack "中 450 {lol} B46 DLFKDLFK dkfdlk D546546    {sdsd} {ds} トカ.ナ  かな  F32     {ds}    B45  {d}  ") of
--     Left a -> print a
--     Right a -> let (m,r,i) = splitKanjiFields $ kanjiInfoFields a in do
--                 putStrLn $ assertEqual (sort $ map reading r) (sort ["かな","トカ.ナ"])
--                 putStrLn $ assertEqual (sort $ map meaning m) (sort ["lol","sdsd","ds","ds","d"])
--                 putStrLn $ assertEqual (sort $ map info i) (sort [Unknown "DLFKDLFK", Unknown "dkfdlk", Unknown "D546546", BushuNumber 46,BushuNumber 45, Frequency 32])
--
-- test_kanji_meanings_with_two_lines =
--   case parse parseKANJIDICLine "" (T.pack "中 450 {lol} B46 DLFKDLFK dkfdlk \n東 420 D546546    {sdsd} {ds} トカ.ナ  かな  F32     {ds}    B45  {d}  ") of
--     Left a -> print a
--     Right a -> let (m,r,i) = splitKanjiFields $ kanjiInfoFields a in do
--                 putStrLn $ assertEqual (sort $ map reading r) (sort [])
--                 putStrLn $ assertEqual (sort $ map meaning m) (sort ["lol"])
--                 putStrLn $ assertEqual (sort $ map info i) (sort [Unknown "DLFKDLFK", Unknown "dkfdlk",  BushuNumber 46])
