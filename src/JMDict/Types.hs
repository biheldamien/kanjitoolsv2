module JMDict.Types
where

import Data.Text

newtype JMDict = JMDict {entries :: [Entry]} deriving (Show)

data Entry = Entry {seqNumber :: Int, kanjiElements:: [KanjiElement], readingElements::[ReadingElement], sense :: Sense} deriving (Show)

data KanjiElement = KanjiElement {word :: Text, kanjiInformations :: [Entity], kanjiPriorities :: [Priority]} deriving (Show) 

data Entity = Ateji | IrregularKanji | IrregularKana | IrregularOkurigana | OutdatedKanji deriving (Show)
data Priority = News1 | News2 | Ichi1 | Ichi2 | Spec1 | Spec2 | Gai1 | Gai2 | Nf Int deriving (Show)
data ReadingElement = ReadingElement {reading :: Text, noKanji :: Bool,
  restrictedKanji :: Maybe Text,
  readingInformations :: [Entity], readingPriorities :: [Priority]} deriving (Show)
data Sense = Sense{
  stagks :: [Text],
  stagrs :: [Text],
  xrefs :: [Text],
  antonymes :: [Text],
  partsOfSpeach :: [Text],
  fields :: [Entity],
  miscs :: [Entity],
  languageSources :: [LanguageSource],
  dialects :: [Entity],
  glosss :: [Gloss],
  senseInformations :: [Text]
  } deriving (Show)

data LanguageSource = LanguageSource {sourceWord :: Text, sourceLang :: Text, fullyDescribed :: Bool, wasei :: Bool} deriving (Show)
newtype Gloss = Gloss {translations :: [Translation]} deriving (Show)
data Translation = Translation {translation :: Text, translationLang :: Text, gender :: Entity, highlighted :: Bool} deriving (Show)

readElement :: Entry -> Text
readElement = undefined
