{-# LANGUAGE OverloadedStrings #-}
module JMDict.Parser 
where

import Text.XML.Stream.Parse
import JMDict.Types
import Data.Conduit
import Data.Text.Read
import Data.XML.Types
import Control.Monad.Trans.Resource

parseJMDictFile :: FilePath -> IO JMDict
parseJMDictFile = undefined
-- parseJMDictFile path = runResourceT $ 
--  parseFile def path $$ parseJMDict

parseJMDict :: MonadThrow m => ConduitM Event o m (Maybe JMDict)
parseJMDict = undefined 
-- parseJMDict =  tagNoAttr "JMdict" $ many parseEntry

parseEntry :: MonadThrow m => ConduitM Event o m (Maybe Entry)
parseEntry = undefined
-- parseEntry =  tagNoAttr "entry" $ parseSeqNumber

parseSeqNumber :: MonadThrow m => ConduitM Event o m (Maybe Int)
parseSeqNumber = undefined
-- parseSeqNumber = tagNoAttr "ent_seq" (content >>= return . either (const 0) (const 4). decimal) 
